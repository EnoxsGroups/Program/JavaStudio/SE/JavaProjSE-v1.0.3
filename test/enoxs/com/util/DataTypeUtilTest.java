package enoxs.com.util;

import enoxs.com.datadef.AppInfo;
import org.junit.Test;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DataTypeUtilTest {

    @Test
    public void convertObject2MapTest() {
        AppInfo appInfo = new AppInfo();
        appInfo.setAppId(Long.valueOf("1"));
        appInfo.setAppName("JavaProjSE");
        appInfo.setAppVersion("1.0.1");
        appInfo.setAppDate("2019-11-15");
        appInfo.setAppAuthor("Enoxs");
        appInfo.setAppRemark("DataTypeUtilTest");


        Map<String, Object> map = DataTypeUtil.convertObject2Map(appInfo);
        String expect, actual;

        expect = Long.valueOf("1") + "";
        actual = map.get("appId") + "";
        assertEquals(expect, actual);

        expect = "JavaProjSE";
        actual = (String) map.get("appName");
        assertEquals(expect, actual);

        expect = "1.0.1";
        actual = (String) map.get("appVersion");
        assertEquals(expect, actual);

        expect = "2019-11-15";
        actual = (String) map.get("appDate");
        assertEquals(expect, actual);

        expect = "Enoxs";
        actual = (String) map.get("appAuthor");
        assertEquals(expect, actual);

        expect = "DataTypeUtilTest";
        actual = (String) map.get("appRemark");
        assertEquals(expect, actual);
    }

    @Test
    public void beanToMapTest() {
        AppInfo appInfo = new AppInfo();
        appInfo.setAppId(Long.valueOf("1"));
        appInfo.setAppName("JavaProjSE");
        appInfo.setAppVersion("1.0.1");
//        appInfo.setAppDate("2019-11-15");
        appInfo.setAppDate(null);
        appInfo.setAppAuthor("Enoxs");
        appInfo.setAppRemark("DataTypeUtilTest01");

        Map<String, Object> map = DataTypeUtil.beanToMap(appInfo);

        System.out.println(map);

        String expect, actual;

        expect = Long.valueOf("1") + "";
        actual = map.get("appId") + "";
        assertEquals(expect, actual);

        expect = "JavaProjSE";
        actual = (String) map.get("appName");
        assertEquals(expect, actual);

        expect = "1.0.1";
        actual = (String) map.get("appVersion");
        assertEquals(expect, actual);

        expect = "";
        actual = (String) map.get("appDate");
        assertEquals(expect, actual);

        expect = "Enoxs";
        actual = (String) map.get("appAuthor");
        assertEquals(expect, actual);

        expect = "DataTypeUtilTest01";
        actual = (String) map.get("appRemark");
        assertEquals(expect, actual);
    }

    @Test
    public void beanToHashtableTest() {
        AppInfo appInfo = new AppInfo();
        appInfo.setAppId(Long.valueOf("1"));
        appInfo.setAppName("JavaProjSE");
        appInfo.setAppVersion("1.0.1");
        appInfo.setAppDate("2019-11-15");
        appInfo.setAppAuthor("Enoxs");
        appInfo.setAppRemark("DataTypeUtilTest");


        Hashtable table = DataTypeUtil.beanToHashtable(appInfo);
        String expect, actual;

        expect = Long.valueOf("1") + "";
        actual = table.get("appId") + "";
        assertEquals(expect, actual);

        expect = "JavaProjSE";
        actual = (String) table.get("appName");
        assertEquals(expect, actual);

        expect = "1.0.1";
        actual = (String) table.get("appVersion");
        assertEquals(expect, actual);

        expect = "2019-11-15";
        actual = (String) table.get("appDate");
        assertEquals(expect, actual);

        expect = "Enoxs";
        actual = (String) table.get("appAuthor");
        assertEquals(expect, actual);

        expect = "DataTypeUtilTest";
        actual = (String) table.get("appRemark");
        assertEquals(expect, actual);
    }


    @Test
    public void convertMap2ObjectTest() {
        Map<String, Object> map = new HashMap<String, Object>() {{
            put("appId", Long.valueOf("1"));
            put("appName", "JavaProjSE");
            put("appVersion", "1.0.1");
            put("appDate", "2019-11-15");
            put("appAuthor", "Enoxs");
            put("appRemark", "DataTypeUtilTest");
        }};

        try {
            AppInfo appInfo = (AppInfo) DataTypeUtil.convertMap2Object(map, AppInfo.class);

            String expect, actual;

            expect = map.get("appId") + "";
            actual = appInfo.getAppId() + "";
            assertEquals(expect, actual);


            expect = map.get("appName") + "";
            actual = appInfo.getAppName() + "";
            assertEquals(expect, actual);

            expect = map.get("appVersion") + "";
            actual = appInfo.getAppVersion() + "";
            assertEquals(expect, actual);

            expect = map.get("appDate") + "";
            actual = appInfo.getAppDate() + "";
            assertEquals(expect, actual);

            expect = map.get("appAuthor") + "";
            actual = appInfo.getAppAuthor() + "";
            assertEquals(expect, actual);

            expect = map.get("appRemark") + "";
            actual = appInfo.getAppRemark() + "";
            assertEquals(expect, actual);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void mapToBean() {
        Map<String, Object> map = new HashMap<String, Object>() {{
            put("appId", Long.valueOf("1"));
            put("appName", "JavaProjSE");
            put("appVersion", "1.0.2");
            put("appDate", "2019-11-15");
            put("appAuthor", "Enoxs");
            put("appRemark", "DataTypeUtilTest");
        }};

        try {
            AppInfo appInfo = (AppInfo) DataTypeUtil.convertMap2Object(map, AppInfo.class);

            String expect, actual;

            expect = map.get("appId") + "";
            actual = appInfo.getAppId() + "";
            assertEquals(expect, actual);


            expect = map.get("appName") + "";
            actual = appInfo.getAppName() + "";
            assertEquals(expect, actual);

            expect = map.get("appVersion") + "";
            actual = appInfo.getAppVersion() + "";
            assertEquals(expect, actual);

            expect = map.get("appDate") + "";
            actual = appInfo.getAppDate() + "";
            assertEquals(expect, actual);

            expect = map.get("appAuthor") + "";
            actual = appInfo.getAppAuthor() + "";
            assertEquals(expect, actual);

            expect = map.get("appRemark") + "";
            actual = appInfo.getAppRemark() + "";
            assertEquals(expect, actual);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void filterMapNullValue() {
        Map<String, Object> map = new LinkedHashMap<String, Object>() {{
            put("A", "A001");
            put("B", "B002");
            put("C", null);
            put("D", "null");
            put("E", "E005");
        }};
        Map<String, Object> actualMap = DataTypeUtil.filterMapNullValue(map);
        String actual = "";
        String expect = "";
        actual = actualMap.get("C") + "";
        assertEquals(expect, actual);

        actual = actualMap.get("D") + "";
        assertEquals(expect, actual);

        AppInfo appInfo = new AppInfo();
        Map<String, Object> appMap = DataTypeUtil.beanToMap(appInfo);
        actual = appMap.get("appId") + "";
        assertEquals(expect, actual);
    }

}