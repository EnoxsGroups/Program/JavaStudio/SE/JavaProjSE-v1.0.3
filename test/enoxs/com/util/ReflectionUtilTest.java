package enoxs.com.util;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class ReflectionUtilTest {

    /**
     * 測試獲取父類的各個方法對象
     */
    @Test
    public void testGetDeclaredMethod(){
        String actual,expect;
        AppInfo appInfo = new AppInfo();

        //獲取公共方法名
        Method publicMethod = ReflectionUtil.getDeclaredMethod(appInfo, "publicMethod");
        System.out.println(publicMethod.getName());

        actual = publicMethod.getName();
        expect = "publicMethod";
        assertEquals(expect,actual);

        //獲取默認方法名
        Method defaultMethod = ReflectionUtil.getDeclaredMethod(appInfo, "defaultMethod");
        System.out.println(defaultMethod.getName());

        actual = defaultMethod.getName();
        expect = "defaultMethod";
        assertEquals(expect,actual);

        //獲取被保護方法名
        Method protectedMethod = ReflectionUtil.getDeclaredMethod(appInfo, "protectedMethod");
        System.out.println(protectedMethod.getName());

        actual = protectedMethod.getName();
        expect = "protectedMethod";
        assertEquals(expect,actual);

        //獲取私有方法名
        Method privateMethod = ReflectionUtil.getDeclaredMethod(appInfo, "privateMethod");
        System.out.println(privateMethod.getName());

        actual = privateMethod.getName();
        expect = "privateMethod";
        assertEquals(expect,actual);
    }

    /**
     * 測試調用父類的方法
     */
    @Test
    public void testInvokeMethod(){
        String actual,expect;
        AppInfo appInfo = new AppInfo();

        //調用父類的公共方法
        actual = (String) ReflectionUtil.invokeMethod(appInfo, "publicMethod", null, null);
        expect = "publicMethod...";
        assertEquals(expect,actual);

        //調用父類的默認方法
        actual = (String) ReflectionUtil.invokeMethod(appInfo, "defaultMethod", null, null);
        expect = "defaultMethod...";
        assertEquals(expect,actual);

        //調用父類的被保護方法
        actual = (String) ReflectionUtil.invokeMethod(appInfo, "protectedMethod", null, null);
        expect = "protectedMethod...";
        assertEquals(expect,actual);

        //調用父類的私有方法
        actual = (String) ReflectionUtil.invokeMethod(appInfo, "privateMethod", null, null);
        expect = "privateMethod...";
        assertEquals(expect,actual);
    }
    /**
     * 測試獲取父類的各個屬性名
     */
    @Test
    public void testGetDeclaredField() {
        String actual,expect;
        AppInfo appInfo = new AppInfo();

        //獲取公共屬性名
        Field publicField = ReflectionUtil.getDeclaredField(appInfo, "publicField");
        System.out.println(publicField.getName());
        actual = publicField.getName();
        expect = "publicField";
        assertEquals(expect,actual);

        //獲取公共屬性名
        Field defaultField = ReflectionUtil.getDeclaredField(appInfo, "defaultField");
        System.out.println(defaultField.getName());
        actual = defaultField.getName();
        expect = "defaultField";
        assertEquals(expect,actual);

        //獲取公共屬性名
        Field protectedField = ReflectionUtil.getDeclaredField(appInfo, "protectedField");
        System.out.println(protectedField.getName());
        actual = protectedField.getName();
        expect = "protectedField";
        assertEquals(expect,actual);

        //獲取公共屬性名
        Field privateField = ReflectionUtil.getDeclaredField(appInfo, "privateField");
        System.out.println(privateField.getName());
        actual = privateField.getName();
        expect = "privateField";
        assertEquals(expect,actual);
    }

    @Test
    public void testGetFieldValue() {
        String [] actual = new String[4];
        String [] expect = new String[4];
        AppInfo appInfo = new AppInfo();

        System.out.println("publicField = " + ReflectionUtil.getFieldValue(appInfo, "publicField"));
        System.out.println("defaultField = " + ReflectionUtil.getFieldValue(appInfo, "defaultField"));
        System.out.println("protectedField = " + ReflectionUtil.getFieldValue(appInfo, "protectedField"));
        System.out.println("privateField = " + ReflectionUtil.getFieldValue(appInfo, "privateField"));

        actual[0] = (String) ReflectionUtil.getFieldValue(appInfo, "publicField");
        actual[1] = (String) ReflectionUtil.getFieldValue(appInfo, "defaultField");
        actual[2] = (String) ReflectionUtil.getFieldValue(appInfo, "protectedField");
        actual[3] = (String) ReflectionUtil.getFieldValue(appInfo, "privateField");
        expect[0] = "1";
        expect[1] = "2";
        expect[2] = "3";
        expect[3] = "4";
        assertArrayEquals(expect,actual);
    }

    @Test
    public void testSetFieldValue() {
        String [] actual = new String[4];
        String [] expect = new String[4];
        AppInfo appInfo = new AppInfo();

        System.out.println("原來的各個屬性的值: ");
        System.out.println("publicField = " + ReflectionUtil.getFieldValue(appInfo, "publicField"));
        System.out.println("defaultField = " + ReflectionUtil.getFieldValue(appInfo, "defaultField"));
        System.out.println("protectedField = " + ReflectionUtil.getFieldValue(appInfo, "protectedField"));
        System.out.println("privateField = " + ReflectionUtil.getFieldValue(appInfo, "privateField"));

        actual[0] = (String) ReflectionUtil.getFieldValue(appInfo, "publicField");
        actual[1] = (String) ReflectionUtil.getFieldValue(appInfo, "defaultField");
        actual[2] = (String) ReflectionUtil.getFieldValue(appInfo, "protectedField");
        actual[3] = (String) ReflectionUtil.getFieldValue(appInfo, "privateField");
        expect[0] = "1";
        expect[1] = "2";
        expect[2] = "3";
        expect[3] = "4";
        assertArrayEquals(expect,actual);

        ReflectionUtil.setFieldValue(appInfo, "publicField", "A");
        ReflectionUtil.setFieldValue(appInfo, "defaultField", "B");
        ReflectionUtil.setFieldValue(appInfo, "protectedField", "C");
        ReflectionUtil.setFieldValue(appInfo, "privateField", "D");

        System.out.println("***********************************************************");

        System.out.println("將屬性值改變後的各個屬性值: ");
        System.out.println("publicField = " + ReflectionUtil.getFieldValue(appInfo, "publicField"));
        System.out.println("defaultField = " + ReflectionUtil.getFieldValue(appInfo, "defaultField"));
        System.out.println("protectedField = " + ReflectionUtil.getFieldValue(appInfo, "protectedField"));
        System.out.println("privateField = " + ReflectionUtil.getFieldValue(appInfo, "privateField"));

        actual[0] = (String) ReflectionUtil.getFieldValue(appInfo, "publicField");
        actual[1] = (String) ReflectionUtil.getFieldValue(appInfo, "defaultField");
        actual[2] = (String) ReflectionUtil.getFieldValue(appInfo, "protectedField");
        actual[3] = (String) ReflectionUtil.getFieldValue(appInfo, "privateField");
        expect[0] = "A";
        expect[1] = "B";
        expect[2] = "C";
        expect[3] = "D";
        assertArrayEquals(expect,actual);
    }

    @Test
    public void changeObjectAttr(){
        Infos infos = new Infos();

        //根據 對象和屬性名通過反射 調用上面的方法獲取 Field對象
        Field field = ReflectionUtil.getDeclaredField(infos, "privateField") ;

        //抑制Java對其的檢查
        field.setAccessible(true) ;

        try {
            field.setInt(null, 1);
            //將 object 中 field 所代表的值 設置為 value
//            field.set(infos, 100) ;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }





    public class Infos{
        public String publicField  = "1";

        String defaultField = "2";

        protected String protectedField = "3";

        private String privateField = "4" ;

        public String publicMethod() {
            System.out.println("publicMethod...");
            return "publicMethod...";
        }

        String defaultMethod() {
            System.out.println("defaultMethod...");
            return "defaultMethod...";
        }

        protected String protectedMethod() {
            System.out.println("protectedMethod...");
            return "protectedMethod...";
        }

        private String privateMethod() {
            System.out.println("privateMethod...");
            return "privateMethod...";
        }
    }

    public class AppInfo extends Infos{

    }
}

