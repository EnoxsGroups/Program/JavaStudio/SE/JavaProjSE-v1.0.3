package enoxs.com.util;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

public class FileUtilTest {
    String rootPath = "assets/FileUtil";
    String separator = File.separator;
    String[] createDirectoryPath = {
            "status",
            "create",
            "rename",
            "copy",
            "delete",
            "delete" + separator + "deleteDirectory",
            "delete" + separator + "deleteDirectoryMulit",
            "delete" + separator + "deleteDirectoryMulit/subDeleteDirectory"
    };
    String[] createFilePath = {
            "status" + separator + "file.txt",
            "rename" + separator + "rename_v1.txt",
            "delete" + separator + "delete.txt",
            "delete" + separator + "deleteDirectoryMulit/subDelete.txt",
            "delete" + separator + "deleteDirectoryMulit/subDeleteDirectory/delete.txt",

    };
    String[] removeDirectory = {
            "create" + separator + "makeDirectory"

    };
    String[] removeFilePath = {
            "create" + separator + "makeFile.txt",
            "create" + separator + "save.txt",
            "rename" + separator + "rename_v2.txt",
            "copy" + separator + "copy.txt"

    };
    Map<String, String> textFile = new LinkedHashMap<String, String>() {{
        put("status" + separator + "load.txt", "File Text Message.");
    }};

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @BeforeClass
    public static void beforeClass() throws IOException {
        FileUtilTest test = new FileUtilTest();
        test.initDirectory();
        test.initFile();
        test.initTextFile();
    }

    @Test
    public void isExist() {
        String path = rootPath + separator + "status" + separator + "file.txt";
        File file = new File(path);
        Boolean isTrue = FileUtil.isExist(file);
        assertTrue(isTrue);

        path = rootPath + separator + "status" + separator + "file.md";
        file = new File(path);
        Boolean isFalse = FileUtil.isExist(file);
        assertFalse(isFalse);
    }

    @Test
    public void isFile() {
        String path = rootPath + separator + "status" + separator + "file.txt";
        File file = new File(path);
        Boolean isTrue = FileUtil.isFile(file);
        assertTrue(isTrue);

        path = rootPath + separator + "status";
        file = new File(path);
        Boolean isFalse = FileUtil.isFile(file);
        assertFalse(isFalse);
    }

    @Test
    public void isDirectory() {
        String path = rootPath + separator + "status";
        File file = new File(path);
        Boolean isTrue = FileUtil.isDirectory(file);
        assertTrue(isTrue);

        path = rootPath + separator + "status" + separator + "file.txt";
        file = new File(path);
        Boolean isFalse = FileUtil.isDirectory(file);
        assertFalse(isFalse);
    }

    @Test
    public void makeDirectory() {
        String path = rootPath + separator + "create" + separator + "makeDirectory";
        File file = new File(path);
        Boolean isFalse = file.exists();
        assertFalse(isFalse);

        FileUtil.makeDirectory(file);

        Boolean isTrue = file.exists();
        assertTrue(isTrue);
    }

    @Test
    public void makeFile() {
        String path = rootPath + separator + "create" + separator + "makeFile.txt";
        File file = new File(path);
        Boolean isFalse = file.exists();
        assertFalse(isFalse);

        FileUtil.makeFile(file);

        Boolean isTrue = file.exists();
        assertTrue(isTrue);
    }

    @Test
    public void save() {
        String path = rootPath + separator + "create" + separator + "save.txt";
        File file = new File(path);
        long actual = file.length();
        long expect = 0;
        assertEquals(expect, actual);

        String msg = "Conserve Message.";
        FileUtil.save(file, msg.getBytes());

        actual = file.length();
        expect = msg.length();
        assertEquals(expect, actual);
    }

    @Test
    public void load() {
        String path = rootPath + separator + "status" + separator + "load.txt";
        File file = new File(path);
        String actual = new String(FileUtil.load(file));
        String expect = "File Text Message.";
        assertEquals(expect, actual);
    }

    @Test
    public void rename() {
        String path = rootPath + separator + "rename" + separator + "rename_v1.txt";
        File file = new File(path);

        Boolean isTrue = file.exists();
        assertTrue(isTrue);

        isTrue = FileUtil.rename("rename_v2", file);
        assertTrue(isTrue);

        Boolean isFalse = file.exists();
        assertFalse(isFalse);

        path = rootPath + separator + "rename" + separator + "rename_v2.txt";
        file = new File(path);
        isTrue = file.exists();
        assertTrue(isTrue);
    }

    @Test
    public void copy() {
        String path = rootPath + separator + "status" + separator + "load.txt";
        File source = new File(path);

        path = rootPath + separator + "copy" + separator + "copy.txt";
        File target = new File(path);

        Boolean isTrue = source.exists();
        assertTrue(isTrue);

        Boolean isFalse = target.exists();
        assertFalse(isFalse);

        FileUtil.copy(target, source);

        isTrue = source.exists();
        assertTrue(isTrue);

        isTrue = target.exists();
        assertTrue(isTrue);
    }

    @Test
    public void delete() {
        String path = rootPath + separator + "delete" + separator + "delete.txt";
        File file = new File(path);
        Boolean isTrue = file.exists();
        assertTrue(isTrue);

        FileUtil.delete(file);

        Boolean isFalse = file.exists();
        assertFalse(isFalse);
    }

    @Test
    public void deleteDirectory() {
        String path = rootPath + separator + "delete" + separator + "deleteDirectory";
        File file = new File(path);
        Boolean isTrue = file.exists();
        assertTrue(isTrue);

        FileUtil.delete(file);

        Boolean isFalse = file.exists();
        assertFalse(isFalse);
    }

    @Test
    public void getPathDirectory() {
        String path = rootPath;
        List<File> lstFile = FileUtil.getPathDirectory(path);
        assertTrue(lstFile.size() > 0);
        for (File file : lstFile) {
            if (file.isDirectory()) {
                assertTrue(FileUtil.isDirectory(file));
            } else {
                assertTrue(FileUtil.isFile(file));
            }
        }
    }

    @Test
    public void createTime() throws ParseException {
        String path = rootPath + separator + "status" + separator + "file.txt";
        File file = new File(path);
        Long time = FileUtil.createTime(file);
        Boolean isTrue = false;
        isTrue = time > sdf.parse("1970-01-01 08:00:00").getTime();
        assertTrue(isTrue);
    }

    @Test
    public void modifyTime() throws ParseException {
        String path = rootPath + separator + "status" + separator + "file.txt";
        File file = new File(path);
        Long time = FileUtil.modifyTime(file);
        Boolean isTrue = false;
        isTrue = time > sdf.parse("1970-01-01 08:00:00").getTime();
        assertTrue(isTrue);
    }

    @Test
    public void accessTime() throws ParseException {
        String path = rootPath + separator + "status" + separator + "file.txt";
        File file = new File(path);
        Long time = FileUtil.accessTime(file);
        Boolean isTrue = false;
        isTrue = time > sdf.parse("1970-01-01 08:00:00").getTime();
        assertTrue(isTrue);
    }

    /**
     * init Data Value
     */

    private void initDirectory() {
        File file;
        for (int i = 0; i < createDirectoryPath.length; i++) {
            file = new File(rootPath + separator + createDirectoryPath[i]);
            if (!file.exists()) {
                file.mkdir();
            }
        }
        for (int i = 0; i < removeDirectory.length; i++) {
            file = new File(rootPath + separator + removeDirectory[i]);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    private void initFile() throws IOException {
        File file;
        for (int i = 0; i < createFilePath.length; i++) {
            file = new File(rootPath + separator + createFilePath[i]);
            if (!file.exists()) {
                file.createNewFile();
            }
        }
        for (int i = 0; i < removeFilePath.length; i++) {
            file = new File(rootPath + separator + removeFilePath[i]);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    private void initTextFile() throws IOException {
        File file;
        for (String key : textFile.keySet()) {
            file = new File(rootPath + separator + key);
            if (!file.exists()) {
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(textFile.get(key));
                bw.close();
            }
        }
    }
}