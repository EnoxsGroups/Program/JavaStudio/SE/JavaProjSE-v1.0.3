package enoxs.com.util;

import org.junit.Test;
import java.util.Properties;

public class ConfigUtilTest {
    @Test
    public void getAppSetting(){
        Properties props = ConfigUtil.loadConfigProps("res/config/config.properties");
        System.out.println("Project : " + props.getProperty("app_name"));
        System.out.println("Version : " + props.getProperty("app_version"));
        System.out.println("Data : " + props.getProperty("app_data"));
        System.out.println("Author : " + props.getProperty("app_author"));
        System.out.println("Remark : " + props.getProperty("app_remark"));
    }
}