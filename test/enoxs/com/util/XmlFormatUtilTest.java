package enoxs.com.util;

import enoxs.com.datadef.AppGroup;
import enoxs.com.datadef.AppGroupInfo;
import enoxs.com.datadef.AppInfo;
import org.junit.Test;

import javax.management.ObjectName;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class XmlFormatUtilTest {

    @Test
    public void ObjectFormatTest01() {
        String expect =
                "<row>" +
                        "<appAuthor name='appAuthor'>Enoxs</appAuthor>" +
                        "<appVersion name='appVersion'>1.0.1</appVersion>" +
                        "<appName name='appName'>JavaProjSE</appName>" +
                        "<appId name='appId'>1</appId>" +
                        "<appDate name='appDate'>2019-11-15</appDate>" +
                        "<appRemark name='appRemark'>Java Project Sample Exampl</appRemark>" +
                        "</row>";
        String actual = "";
        AppInfo appInfo = new AppInfo();
        appInfo.setAppId(Long.valueOf("1"));
        appInfo.setAppName("JavaProjSE");
        appInfo.setAppVersion("1.0.1");
        appInfo.setAppDate("2019-11-15");
        appInfo.setAppAuthor("Enoxs");
        appInfo.setAppRemark("Java Project Sample Exampl");
        Map<String, Object> map = DataTypeUtil.convertObject2Map(appInfo);
        actual = XmlFormatUtil.formatXML(map);
        assertEquals(expect, actual);
    }

    @Test
    public void ListObjectFormatTest02() {
        AppInfo appInfo01 = new AppInfo();
        appInfo01.setAppId(Long.valueOf("1"));
        appInfo01.setAppName("JavaProjSE");
        appInfo01.setAppVersion("1.0.1");
        appInfo01.setAppDate("2019-11-15");
        appInfo01.setAppAuthor("Enoxs");
        appInfo01.setAppRemark("Java Project Sample Exampl");
        AppInfo appInfo02 = new AppInfo();
        appInfo02.setAppId(Long.valueOf("1"));
        appInfo02.setAppName("JUnitSE");
        appInfo02.setAppVersion("1.0.2");
        appInfo02.setAppDate("2019-11-18");
        appInfo02.setAppAuthor("Enoxs");
        appInfo02.setAppRemark("Java Unit Test Sample Exampl");
        List<AppInfo> lstAppInfo = new LinkedList<AppInfo>() {{
            add(appInfo01);
            add(appInfo02);
        }};
        Map<String, Object> map = new HashMap<String, Object>() {{
            put("lstAppInfo", lstAppInfo);
        }};
        String expect =
                "<row>" +
                    "<lstAppInfo name='lstAppInfo'>" +
                        "<list name='list'>" +
                        "<appAuthor name='appAuthor'>Enoxs</appAuthor>" +
                        "<appVersion name='appVersion'>1.0.1</appVersion>" +
                        "<appName name='appName'>JavaProjSE</appName>" +
                        "<appId name='appId'>1</appId>" +
                        "<appDate name='appDate'>2019-11-15</appDate>" +
                        "<appRemark name='appRemark'>Java Project Sample Exampl</appRemark>" +
                        "</list>" +
                        "<list name='list'>" +
                        "<appAuthor name='appAuthor'>Enoxs</appAuthor>" +
                        "<appVersion name='appVersion'>1.0.2</appVersion>" +
                        "<appName name='appName'>JUnitSE</appName>" +
                        "<appId name='appId'>1</appId>" +
                        "<appDate name='appDate'>2019-11-18</appDate>" +
                        "<appRemark name='appRemark'>Java Unit Test Sample Exampl</appRemark>" +
                        "</list>" +
                    "</lstAppInfo>" +
                "</row>";
        String actual = XmlFormatUtil.formatXML(map);
        System.out.println(actual);
        assertEquals(expect, actual);
    }


    @Test
    public void ListObjectIncludeListObject() {
        String expect =
        "<row>"+
            "<lstAppGroupInfo name='lstAppGroupInfo'>"+
                "<list name='list'>"+
                    "<lstAppInfo name='lstAppInfo'>"+
                        "<list name='list'>"+
                            "<appAuthor name='appAuthor'>Enoxs</appAuthor>"+
                            "<appVersion name='appVersion'>1.0.1</appVersion>"+
                            "<appName name='appName'>JavaProjSE</appName>"+
                            "<appId name='appId'>1</appId>"+
                            "<appDate name='appDate'>2019-11-15</appDate>"+
                            "<appRemark name='appRemark'>Java Project Sample Exampl</appRemark>"+
                        "</list>"+
                        "<list name='list'>"+
                            "<appAuthor name='appAuthor'>Enoxs</appAuthor>"+
                            "<appVersion name='appVersion'>1.0.2</appVersion>"+
                            "<appName name='appName'>JUnitSE</appName>"+
                            "<appId name='appId'>1</appId>"+
                            "<appDate name='appDate'>2019-11-18</appDate>"+
                            "<appRemark name='appRemark'>Java Unit Test Sample Exampl</appRemark>"+
                        "</list>"+
                    "</lstAppInfo>"+
                    "<name name='name'>JavaStudio</name>"+
                    "<description name='description'>Java Project</description>"+
                    "<id name='id'>1</id>"+
                "</list>"+
                "<list name='list'>"+
                    "<lstAppInfo name='lstAppInfo'>null</lstAppInfo>"+
                    "<name name='name'>AndroidStudio</name>"+
                    "<description name='description'>Android Project</description>"+
                    "<id name='id'>2</id>"+
                "</list>"+
            "</lstAppGroupInfo>"+
        "</row>";

        AppInfo appInfo01 = new AppInfo();
        appInfo01.setAppId(Long.valueOf("1"));
        appInfo01.setAppName("JavaProjSE");
        appInfo01.setAppVersion("1.0.1");
        appInfo01.setAppDate("2019-11-15");
        appInfo01.setAppAuthor("Enoxs");
        appInfo01.setAppRemark("Java Project Sample Exampl");
        AppInfo appInfo02 = new AppInfo();
        appInfo02.setAppId(Long.valueOf("1"));
        appInfo02.setAppName("JUnitSE");
        appInfo02.setAppVersion("1.0.2");
        appInfo02.setAppDate("2019-11-18");
        appInfo02.setAppAuthor("Enoxs");
        appInfo02.setAppRemark("Java Unit Test Sample Exampl");
        List<AppInfo> lstAppInfo = new LinkedList<AppInfo>() {{
            add(appInfo01);
            add(appInfo02);
        }};
        AppGroupInfo appGroupInfo01 = new AppGroupInfo();
        appGroupInfo01.setId(Long.valueOf(1));
        appGroupInfo01.setName("JavaStudio");
        appGroupInfo01.setDescription("Java Project");
        appGroupInfo01.setLstAppInfo(lstAppInfo);

        AppGroupInfo appGroupInfo02 = new AppGroupInfo();
        appGroupInfo02.setId(Long.valueOf(2));
        appGroupInfo02.setName("AndroidStudio");
        appGroupInfo02.setDescription("Android Project");

        List<AppGroupInfo> lstAppGroupInfo = new LinkedList<>();
        lstAppGroupInfo.add(appGroupInfo01);
        lstAppGroupInfo.add(appGroupInfo02);

        Map<String, Object> map = new HashMap<String, Object>() {{
            put("lstAppGroupInfo", lstAppGroupInfo);
        }};
        String actual = XmlFormatUtil.formatXML(map);
        System.out.println(actual);
        assertEquals(expect, actual);

    }
}