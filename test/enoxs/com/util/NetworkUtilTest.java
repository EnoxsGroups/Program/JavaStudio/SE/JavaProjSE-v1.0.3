package enoxs.com.util;

import org.junit.Test;
import sun.nio.ch.Net;

import static org.junit.Assert.*;

public class NetworkUtilTest {

    @Test
    public void checkUrlConnStatus() {
        String url = "https://www.google.com/";
        Boolean isTrue = NetworkUtil.checkUrlConnStatus(url);
        assertTrue(isTrue);
    }
}