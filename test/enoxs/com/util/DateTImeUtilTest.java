package enoxs.com.util;

import enoxs.com.datadef.AppInfo;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class DateTImeUtilTest {

    @Test
    public void convert2MinDate() {
        String expect = "2019-11-15";
        String actual = DateTimeUtil.convert2Date("20191115");
        assertEquals(expect, actual);
    }

    @Test
    public void minDateConvert2Date() {
        String expect = "20191115";
        String actual = DateTimeUtil.convert2MinDate("2019-11-15");
        assertEquals(expect, actual);
    }




    @Test
    public void sortSequenceByDateTest(){
        AppInfo appInfo01 = new AppInfo();
        appInfo01.setAppId(Long.valueOf(1));
        appInfo01.setAppName("Sort01");
        appInfo01.setAppDate("2019-01-01");

        AppInfo appInfo02 = new AppInfo();
        appInfo02.setAppId(Long.valueOf(2));
        appInfo02.setAppName("Sort02");
        appInfo02.setAppDate("2019-01-03");

        AppInfo appInfo03 = new AppInfo();
        appInfo03.setAppId(Long.valueOf(10));
        appInfo03.setAppName("Sort04");
        appInfo03.setAppDate("2019-12-01");

        AppInfo appInfo04 = new AppInfo();
        appInfo04.setAppId(Long.valueOf(12));
        appInfo04.setAppName("Sort05");
        appInfo04.setAppDate("2019-12-30");

        AppInfo appInfo05 = new AppInfo();
        appInfo05.setAppId(Long.valueOf(5));
        appInfo05.setAppName("Sort03");
        appInfo05.setAppDate("2019-09-12");

        Map<String,AppInfo> map = new HashMap<>();
        map.put(appInfo03.getAppId()+"" , appInfo03 );
        map.put(appInfo01.getAppId()+"" , appInfo01);
        map.put(appInfo04.getAppId()+"" , appInfo04);
        map.put(appInfo05.getAppId()+"" , appInfo05);
        map.put(appInfo02.getAppId()+"" , appInfo02);

        Map <String,String> seq = new HashMap<>();
        for (String code : map.keySet()) {
            seq.put(code,map.get(code).getAppDate());
        }

        String [] seqSort = DateTimeUtil.sortSequenceByDate(seq);

        System.out.println(Arrays.toString(seqSort));
        String expect,actual;
        String [] expects = { "1","2","5","10","12"};
        for (int i = 0; i < seqSort.length; i++) {
            String code = seqSort[i];
            expect = expects[i];
            actual = map.get(code).getAppId() + "";
            assertEquals(expect,actual);
        }
    }



}