package enoxs.com.util;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Log4jUtilTest {
    private static Logger log;
    @Before
    public void setUp() throws Exception {
        Log4jUtil.loadLog4jProps("res/config/log4j/log4j.properties");
        log = Logger.getLogger(Log4jUtilTest.class);
    }
    @Test
    public void testLogInfo(){
        log.trace("OwO / ");
        log.info("OwO / ");
        log.debug("OwO / ");
        log.warn("OwO / ");
        log.error("OwO / ");
    }
}