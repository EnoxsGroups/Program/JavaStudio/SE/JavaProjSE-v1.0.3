package enoxs.com.util;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.util.Hashtable;
import java.util.Vector;

import static org.junit.Assert.*;

public class ParameterPrintUtilTest {

    @BeforeClass
    public static void beforeClass(){
        String path = "/Users/richard/Mac_Document/Workspace/DevLab/Log/print.log";
        ParameterPrintUtil.setPath(path);
    }

    @Test
    public void print() {
        Hashtable htParam = new Hashtable();
        htParam.put("A", "A001");
        htParam.put("B", "B001");
        htParam.put("C", "C001");
        htParam.put("D", "D001");
        htParam.put("E", "E001");
        Vector vParam = new Vector();
        vParam.add("R01");
        vParam.add("R02");
        vParam.add("R03");
        vParam.add(htParam);
        htParam.put("V", vParam);
        ParameterPrintUtil.print("getListResult", htParam);
    }

    @Test
    public void print1() {
        Hashtable htParam = new Hashtable();
        htParam.put("A", "A001");
        htParam.put("B", "B001");
        htParam.put("C", "C001");
        htParam.put("D", "D001");
        htParam.put("E", "E001");
        Vector vParam = new Vector();
        vParam.add("R01");
        vParam.add("R02");
        vParam.add("R03");
        vParam.add(htParam);
        ParameterPrintUtil.print("getListResult", vParam);
    }

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(ParameterPrintUtilTest.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}