package enoxs.com.encrypt;

import org.junit.Test;

import static org.junit.Assert.*;

public class Base64UtilTest {

    @Test
    public void encode() {
    }

    @Test
    public void decode() {
        String code = "YWRtaW46MDAwMA==";
        String show = Base64Util.decode(code);
        System.out.println("show() = " + show);
    }

    @Test
    public void base64EncryptTest(){
        final String text = "字串文字";
        String msg = "";

        msg = Base64Util.encode(text);
        System.out.println(msg);

        msg = Base64Util.decode(msg);
        System.out.println(msg);
    }
}