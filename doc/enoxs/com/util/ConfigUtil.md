ConfigUtil
======

功能說明
------
Java 配置檔載入。

使用 java.util.Properties 類別,讀取配置檔(.properties) 內容。

檔案路徑
------
+ java: src/enoxs/com/util/ConfigUtil.java
+ test: test/enoxs/com/util/ConfigUtilTest.java
+ doc : doc/enoxs/com/util/ConfigUtil.md
+ res : res/config/config.properties

使用說明
------
範例： ConfigUtilTest

功能：
+ 讀取配置檔內容
    + 方法 : loadConfigProps(configPath)
    + 輸入 : 檔案路徑
    + 返回 : Properties 類別
