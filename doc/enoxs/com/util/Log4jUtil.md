Log4jUtil
======

功能說明
------
Apache Log4j 日誌紀錄工具封裝

檔案路徑
------
+ java: src/enoxs/com/util/Log4jUtil.java
+ test: test/enoxs/com/util/Log4jUtilTest.java
+ doc : doc/enoxs/com/util/Log4jUtil.md
+ res : res/config/log4j/log4j.properties
+ log : log/msg.log

使用說明
------
範例： Log4jUtilTest

功能：
+ 初始化 Log4j
    + 方法 : loadLog4jProps(configPath)
    + 輸入 : 配置檔路徑

使用：

初始完畢後，在要記錄日誌類別，創建 log4j 物件。
```
private static Logger log = Logger.getLogger(ClassName.class);
```
使用物件依據層級紀錄
```
log.info("OwO / ");
```
日誌輸出路徑，依據 log4j.properties 配置，目前路徑為

    log/msg.log
        
依賴：
+ log4j-1.2.16.jar
+ ConfigUtil.class