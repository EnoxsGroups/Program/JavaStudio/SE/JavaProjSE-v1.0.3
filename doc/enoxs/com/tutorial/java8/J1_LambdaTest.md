Lambda Expressions
======

Reference
-----
### Java 8 - Lambda Expressions

### URL
<https://www.tutorialspoint.com/java8/java8_lambda_expressions.htm>

Syntax
------
A lambda expression is characterized by the following syntax.
    
    parameter -> expression body

Following are the important characteristics of a lambda expression.

### Optional type declaration 
No need to declare the type of a parameter. The compiler can inference the same from the value of the parameter.

### Optional parenthesis around parameter 
No need to declare a single parameter in parenthesis. For multiple parameters, parentheses are required.

### Optional curly braces 
No need to use curly braces in expression body if the body contains a single statement.

### Optional return keyword 
The compiler automatically returns the value if the body has a single expression to return the value. Curly braces are required to indicate that expression returns a value.r automatically returns the value if the body has a single expression to return the value. Curly braces are required to indicate that expression returns a value.

Important Point
------
+ Lambda expressions are used primarily to define inline implementation of a functional interface.
+ Lambda expression eliminates the need of anonymous class and gives a very simple yet powerful functional programming capability to Java.