package enoxs.com.exec;

public class JarRunner {
    /**
     * setting artifacts in project structure
     * build artifacts -> export .jar
     * cd out/artifacts/
     * java -jar JavaProjSE-v1.0.3.jar "001" "002" "003"
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer(64);
        sb.append("Java Project Simple Example Version 1.0.3 - JarRunner");
        sb.append("\n");
        sb.append("Length:");
        sb.append(args.length);
        sb.append("\n");
        for (int i = 0; i < args.length; i++) {
            sb.append((i+1));
            sb.append(" : ");
            sb.append(args[i]);
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
}
