package enoxs.com.datadef;

public class AppInfo {
    private Long appId;

    private String appName;

    private String appVersion;

    private String appDate;

    private String appAuthor;

    private String appRemark;

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getAppAuthor() {
        return appAuthor;
    }

    public void setAppAuthor(String appAuthor) {
        this.appAuthor = appAuthor;
    }

    public String getAppRemark() {
        return appRemark;
    }

    public void setAppRemark(String appRemark) {
        this.appRemark = appRemark;
    }

    @Override
    public String toString() {
        return "AppInfo{" +
                "appId=" + appId +
                ", appName='" + appName + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", appDate='" + appDate + '\'' +
                ", appAuthor='" + appAuthor + '\'' +
                ", appRemark='" + appRemark + '\'' +
                '}';
    }
}
