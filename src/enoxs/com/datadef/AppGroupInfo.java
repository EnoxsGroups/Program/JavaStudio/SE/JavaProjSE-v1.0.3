package enoxs.com.datadef;

import java.util.List;

public class AppGroupInfo extends AppGroup{
    private List<AppInfo> lstAppInfo;

    public List<AppInfo> getLstAppInfo() {
        return lstAppInfo;
    }

    public void setLstAppInfo(List<AppInfo> lstAppInfo) {
        this.lstAppInfo = lstAppInfo;
    }
}
