package enoxs.com.tutorial.java.j1_basic;

import org.junit.Test;

public class S2_Constructors {
    public class AppInfoObject{
        private Long appId;
        private String appName;
        private String appVersion;
        private String appDate;
        private String appAuthor;
        private String appRemark;

        public AppInfoObject(){

        }

        public AppInfoObject(Long appId, String appName, String appVersion, String appDate, String appAuthor, String appRemark) {
            this.appId = appId;
            this.appName = appName;
            this.appVersion = appVersion;
            this.appDate = appDate;
            this.appAuthor = appAuthor;
            this.appRemark = appRemark;
        }

        public Long getAppId() {
            return appId;
        }
        public void setAppId(Long appId) {
            this.appId = appId;
        }
        public String getAppName() {
            return appName;
        }
        public void setAppName(String appName) {
            this.appName = appName;
        }
        public String getAppVersion() {
            return appVersion;
        }
        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }
        public String getAppDate() {
            return appDate;
        }
        public void setAppDate(String appDate) {
            this.appDate = appDate;
        }
        public String getAppAuthor() {
            return appAuthor;
        }
        public void setAppAuthor(String appAuthor) {
            this.appAuthor = appAuthor;
        }
        public String getAppRemark() {
            return appRemark;
        }
        public void setAppRemark(String appRemark) {
            this.appRemark = appRemark;
        }

        public void printMsg(){
            StringBuffer sb = new StringBuffer(64);
            sb.append("Id: " + getAppId() + "\n");
            sb.append("Name: " + getAppName() + "\n");
            sb.append("Version: " + getAppVersion() + "\n");
            sb.append("Date: " + getAppDate() + "\n");
            sb.append("Author: " + getAppAuthor() + "\n");
            sb.append("Remark: " + getAppRemark() + "\n");
            System.out.println(sb.toString());
        }
    }
    @Test
    public void testS2_Constructors(){
        AppInfoObject mJavaProjSE = new AppInfoObject();
        AppInfoObject mJUnitSE;

        mJavaProjSE.setAppId(Long.valueOf(1));
        mJavaProjSE.setAppName("JavaProjSE-v1.0.3");
        mJavaProjSE.setAppVersion("1.0.3");
        mJavaProjSE.setAppDate("2019/07/24");
        mJavaProjSE.setAppAuthor("Enoxs");
        mJavaProjSE.setAppRemark("Java Project Simple Example - Version 1.0.3");

        mJUnitSE = new AppInfoObject(
                Long.valueOf(2),
                "JUnitSE",
                "1.0.2",
                "not init",
                "Enoxs",
                "Java Unit Test Simple Example"
        );

        mJavaProjSE.printMsg();
        mJUnitSE.printMsg();
    }
}
