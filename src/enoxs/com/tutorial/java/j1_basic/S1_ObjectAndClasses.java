package enoxs.com.tutorial.java.j1_basic;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class S1_ObjectAndClasses {
    public class AppInfoObject{
        private Long appId;
        private String appName;
        private String appVersion;
        private String appDate;
        private String appAuthor;
        private String appRemark;
        public Long getAppId() {
            return appId;
        }
        public void setAppId(Long appId) {
            this.appId = appId;
        }
        public String getAppName() {
            return appName;
        }
        public void setAppName(String appName) {
            this.appName = appName;
        }
        public String getAppVersion() {
            return appVersion;
        }
        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }
        public String getAppDate() {
            return appDate;
        }
        public void setAppDate(String appDate) {
            this.appDate = appDate;
        }
        public String getAppAuthor() {
            return appAuthor;
        }
        public void setAppAuthor(String appAuthor) {
            this.appAuthor = appAuthor;
        }
        public String getAppRemark() {
            return appRemark;
        }
        public void setAppRemark(String appRemark) {
            this.appRemark = appRemark;
        }
    }

    @Test
    public void testObjectAndClasses(){
        AppInfoObject appInfo = new AppInfoObject();
        appInfo.setAppId(Long.valueOf(1));
        appInfo.setAppName("JavaProjSE-v1.0.3");
        appInfo.setAppVersion("1.0.3");
        appInfo.setAppDate("2019/07/24");
        appInfo.setAppAuthor("Enoxs");
        appInfo.setAppRemark("Java Project Simple Example - Version 1.0.3");

        assertEquals(Long.valueOf(1),appInfo.getAppId());
        assertEquals("JavaProjSE-v1.0.3",appInfo.getAppName());
        assertEquals("1.0.3",appInfo.getAppVersion());
        assertEquals("2019/07/24",appInfo.getAppDate());
        assertEquals("Enoxs",appInfo.getAppAuthor());
        assertEquals("Java Project Simple Example - Version 1.0.3",appInfo.getAppRemark());
    }

}
