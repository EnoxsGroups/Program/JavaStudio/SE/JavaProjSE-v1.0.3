package enoxs.com.tutorial.java.j1_basic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class S4_VariableTypes {
    private String message;

    private Integer number;

    private static double itemCount;

    public static final String ITEM_NAME = "Java";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void printInstanceMsg(){
        System.out.println("number : " + getNumber());
        System.out.println("Message : " + getMessage());
    }

    public void printLocalMsg(){
        String msg = "Message.";
        System.out.println(msg);
    }

    @Test
    public void testDeclareVariable(){
        int a, b, c;         // Declares three ints, a, b, and c.
        int d = 10, e = 10;  // Example of initialization
        byte B = 22;         // initializes a byte type variable B.
        double pi = 3.14159; // declares and assigns a value of PI.
        char g = 'a';        // the char variable a iis initialized with value 'a'
    }

    /**
     * Local variables - 區域變數
     */
    @Test
    public void testLocalVariable(){
        S4_VariableTypes s4 = new S4_VariableTypes();
        s4.printLocalMsg();
    }

    /**
     * Instance variables - 實例變數
     */
    @Test
    public void testInstanceVariables(){
        S4_VariableTypes s4 = new S4_VariableTypes();
        s4.setNumber(1);
        s4.setMessage("Hello");
        s4.printInstanceMsg();
    }

    /**
     * Class/Static variables - 類別/靜態 變數
     */
    @Test
    public void testClassStaticVariables(){
        itemCount = 1000;
        String actual = "Count of " + ITEM_NAME + " : " + itemCount;
        String expect = "Count of Java : 1000.0";
        assertEquals(expect,actual);
    }

}
