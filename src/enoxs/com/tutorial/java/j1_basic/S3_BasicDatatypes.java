package enoxs.com.tutorial.java.j1_basic;

import org.junit.Test;

public class S3_BasicDatatypes {
    @Test
    public void testDataType(){
        byte a01 = 68;
        char a02 = 'A';
        System.out.println("a01: " + a01);
        System.out.println("a02: " + a02);

        int decimal = 100;
        int octal = 0144;
        int hexa =  0x64;
        System.out.println("decimal: " + decimal);
        System.out.println("octal: " + octal);
        System.out.println("hexa: " + hexa);

        char b01 = '\u0001';
        String b02 = "\u0001";
        System.out.println("b01: " + b01);
        System.out.println("b02: " + b02);
    }
}
