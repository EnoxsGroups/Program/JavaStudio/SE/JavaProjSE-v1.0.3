package enoxs.com.tutorial.java8;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class J2_MethodReferenceTest {
    @Test
    public void methodReferenceTest() {
        List names = new ArrayList();

        names.add("Mahesh");
        names.add("Suresh");
        names.add("Ramesh");
        names.add("Naresh");
        names.add("Kalpesh");

        names.forEach(System.out::println);
    }
}
