package enoxs.com.tutorial.java8;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Java8Test {
    private List<String> lstName01;
    private List<String> lstName02;
    @Before
    public void init(){
        lstName01 = new ArrayList<String>();
        lstName01.add("Mahesh ");
        lstName01.add("Suresh ");
        lstName01.add("Ramesh ");
        lstName01.add("Naresh ");
        lstName01.add("Kalpesh ");

        lstName02 = new ArrayList<String>();
        lstName02.add("Mahesh ");
        lstName02.add("Suresh ");
        lstName02.add("Ramesh ");
        lstName02.add("Naresh ");
        lstName02.add("Kalpesh ");
    }

    @Test
    public void java8Test() {
        System.out.println("Sort using Java 7 syntax: ");
        sortUsingJava7(lstName01);
        System.out.println(lstName01);

        System.out.println("Sort using Java 8 syntax: ");
        sortUsingJava8(lstName02);
        System.out.println(lstName02);
    }

    //sort using java 7
    private void sortUsingJava7(List<String> names) {
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });
    }

    //sort using java 8
    private void sortUsingJava8(List<String> names) {
        Collections.sort(names, (s1, s2) -> s1.compareTo(s2));
    }
}
