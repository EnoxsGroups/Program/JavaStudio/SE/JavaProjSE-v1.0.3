package enoxs.com.encrypt;

import org.junit.Test;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Base64Util {
    private final static BASE64Encoder encoder = new BASE64Encoder();
    private final static BASE64Decoder decoder = new BASE64Decoder();

    public static String encode(String text){
        String encodedText = "";
        try {
            byte[] textByte = text.getBytes("UTF-8");
            encodedText = encoder.encode(textByte);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodedText;
    }

    public static String decode(String encodedText){
        String decodeText = "";
        try {
            decodeText = new String(decoder.decodeBuffer(encodedText), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return decodeText;
    }



}
