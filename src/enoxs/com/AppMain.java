package enoxs.com;

public class AppMain {
    public static void main(String args []){
        System.out.println("Java Project Simple Example Version 1.0.3");
    }
    /**
     * Java Ant Tutor
     */
    // Enoxs To-DO :　Ant - Building Projects
    // Enoxs To-DO :　Ant - Build Documentation
    // Enoxs To-DO :　Ant - Creating JAR files
    // Enoxs To-DO :　Ant - Creating WAR files
    // Enoxs To-DO :　Ant - Packaging Applications
    // Enoxs To-DO :　Ant - Deploying Applications
    // Enoxs To-DO :　Ant - Executing Java code
    // Enoxs To-DO :　Ant - Eclipse Integration
    // Enoxs To-DO :　Ant - Junit Integration
    // Enoxs To-DO :　Ant - Extending Ant

    /**
     * To-Do List
     */

    // Enoxs To-Do : SE 十步學習法
    // Enoxs To-Do : SE - Collections can first.

    // Enoxs To-Do : SE - Java  - Tutorials
    // Enoxs To-Do : SE - Java 8 - Tutorials

    // Enoxs To-Do : Exec - bat / sh
    // Enoxs To-Do : JavaProjSE-v1.0.2 Review

    /**
     * Java  - Tutorials
     */
    // Java Tutorial
    // Enoxs Study : Java - Modifier Types
    // Enoxs Study : Java - Basic Operators
    // Enoxs Study : Java - Loop Control
    // Enoxs Study : Java - Decision Making
    // Enoxs Study : Java - Numbers
    // Enoxs Study : Java - Characters
    // Enoxs Study : Java - Strings
    // Enoxs Study : Java - Arrays
    // Enoxs Study : Java - Date & Time
    // Enoxs Study : Java - Regular Expressions
    // Enoxs Study : Java - Methods
    // Enoxs Study : Java - Files and I/O
    // Enoxs Study : Java - Exceptions
    // Enoxs Study : Java - Inner classes
    // Java Object Oriented
    // Enoxs Study : Java - Inheritance
    // Enoxs Study : Java - Overriding
    // Enoxs Study : Java - Polymorphism
    // Enoxs Study : Java - Abstraction
    // Enoxs Study : Java - Encapsulation
    // Enoxs Study : Java - Interfaces
    // Enoxs Study : Java - Packages
    // Java Advanced
    // Enoxs Study Java - Data Structures
    // Enoxs Study Java - Collections
    // Enoxs Study Java - Generics
    // Enoxs Study Java - Serialization
    // Enoxs Study Java - Networking
    // Enoxs Study Java - Sending Email
    // Enoxs Study Java - Multithreading
    // Enoxs Study Java - Applet Basics
    // Enoxs Study Java - Documentation

    /**
     * Review
     */
    // Enoxs To-Do : ByteBuffer
    // Enoxs To-Do : Queue

    // Enoxs To-Do : JDBC - SQLite / MS-SQL / MySQL
    // Enoxs To-Do : Thread
    // Enoxs To-Do : Socket.net
    // Enoxs To-Do : RS232
    // Enoxs To-Do : Component : OOP - Object-Oriented Programming
    // Enoxs To-Do : Tip Package

    // Enoxs Review : example/demo/ArraysDemo
    // Enoxs Review : example/demo/ArraysDemo
    // Enoxs Review : example/demo/ByteBufferDemo
    // Enoxs Review : example/demo/CalculateDemo
    // Enoxs Review : example/demo/CalendarDemo
    // Enoxs Review : example/demo/CallbackDemo
    // Enoxs Review : example/demo/CollectionDemo
    // Enoxs Review : example/demo/FormatDemo
    // Enoxs Review : example/demo/QueueDemo
    // Enoxs Review : example/demo/SingletonDemo
    // Enoxs Review : example/demo/TipDemo

    // Enoxs Review : example/internet/Client
    // Enoxs Review : example/internet/Server
    // Enoxs Review : example/internet/SocketClient
    // Enoxs Review : example/internet/SocketHello
    // Enoxs Review : example/internet/SocketMonitor
    // Enoxs Review : example/internet/SocketServer

    // Enoxs Review : example/jdbc/MSSQLDemo
    // Enoxs Review : example/jdbc/MySQLDemo
    // Enoxs Review : example/jdbc/SQLiteDemo

    // Enoxs Review : example/oop/AbstractX
    // Enoxs Review : example/oop/InterfaceX
    // Enoxs Review : example/oop/ObjectX1
    // Enoxs Review : example/oop/Parent
    // Enoxs Review : example/oop/Son

    // Enoxs Review : example/rs232/Device.java

    // Enoxs Review : example/thread/MessageConsoleCenter.java
    // Enoxs Review : example/thread/MessageConsoleServer.java
    // Enoxs Review : example/thread/MultiSocketClient.java
    // Enoxs Review : example/thread/TestSynchronizedv1.java
    // Enoxs Review : example/thread/TestSynchronizedv2.java
    // Enoxs Review : example/thread/ThreadBroadcastCenter.java
    // Enoxs Review : example/thread/ThreadSpaceX.java

    // Enoxs Review : utillity/Calculator.java
    // Enoxs Review : utillity/FileOperator.java
    // Enoxs Review : utillity/RXTXComm.java
    // Enoxs Review : utillity/ReflectionUtils.java
    // Enoxs Review : utillity/SettingUtils.java
    // Enoxs Review : utillity/TypeSettingUtils.java
    // Enoxs Review : utillity/VerifyUtils.java

    /**
     * Util
     */
    // Enoxs To-Do : util/FileUtil
    // Enoxs To-Do : JSONUtil - test

    // Enoxs To-Do : util/NetworkUtil - review & tests
    // Enoxs To-Do : util/NetworkUtil 文檔說明

    // Enoxs To-Do : util/FileUtil - review & test
    // Enoxs To-Do : util/FileUtil 文檔說明

    // Enoxs To-Do : util/ReflectionUtil - review & test
    // Enoxs To-Do : util/ReflectionUtil 文檔說明

    // Enoxs To-Do : add CalculateUtil
    // Enoxs To-Do : add RXTXCommUtil
    // Enoxs TO-Do : add VerifySE

    /**
     * Tutorials - Order
     */
    // Enoxs To-Do : Java
    // Enoxs To-Do : Java 8
    // Enoxs To-Do : Java IO
    // Enoxs To-Do : Kotlin
    // Enoxs To-Do : Scala

    /**
     * Tutorials - View
     * URL:
     * https://www.tutorialspoint.com/java_technology_tutorials.htm
     */
    // Enoxs To-Do : Java Technologies , Create To Do - List
}
