package enoxs.com.se.java8.util.stream;

import enoxs.com.datadef.AppInfo;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamSE {

    @Test
    public void filterTest(){
        List<AppInfo> lstAppInfo = new LinkedList<AppInfo>();
        AppInfo app01 = new AppInfo();
        app01.setAppId(Long.valueOf(1));
        app01.setAppName("APP_01");
        app01.setAppVersion("v1.0.1");
        AppInfo app02 = new AppInfo();
        app02.setAppId(Long.valueOf(2));
        app02.setAppName("APP_02");
        app02.setAppVersion("v1.0.1");
        AppInfo app03 = new AppInfo();
        app03.setAppId(Long.valueOf(3));
        app03.setAppName("APP_03");
        app03.setAppVersion("v1.0.2");
        AppInfo app04 = new AppInfo();
        app04.setAppId(Long.valueOf(4));
        app04.setAppName("APP_04");
        app04.setAppVersion("v1.0.2");
        AppInfo app05 = new AppInfo();
        app05.setAppId(Long.valueOf(5));
        app05.setAppName("APP_05");
        app05.setAppVersion("v1.0.3");
        lstAppInfo.add(app01);
        lstAppInfo.add(app02);
        lstAppInfo.add(app03);
        lstAppInfo.add(app04);
        lstAppInfo.add(app05);

        List<AppInfo> lstResult = lstAppInfo.stream().filter(app -> app.getAppVersion().equals("v1.0.2")).collect(Collectors.toList());
        System.out.println("Size: " + lstResult.size() + "\n");

        for (AppInfo appInfo : lstResult) {
            System.out.println("id:" + appInfo.getAppId());
            System.out.println("name:" + appInfo.getAppName());
            System.out.println("version:" + appInfo.getAppVersion());
            System.out.println();
        }

    }

}
