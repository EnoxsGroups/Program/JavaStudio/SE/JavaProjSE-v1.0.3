package enoxs.com.se.pattern;

import org.junit.Test;

public class SingletonSE {
    private static SingletonSE singletonSE = new SingletonSE();
    public static SingletonSE getInstance(){
        return singletonSE;
    }

    private String message;

    public SingletonSE(){
    }

    public void setMessage(String msg){
        this.message = msg;
    }

    public void printMsg(){
        System.out.println(message);
    }
    @Test
    public void singletonTest() {
        SingletonSE singletonSE01 =  SingletonSE.getInstance();
        singletonSE01.setMessage("Hello OwO");
        singletonSE01.printMsg();

        SingletonSE singletonSE02 = SingletonSE.getInstance();
        singletonSE02.printMsg();

        singletonSE02.setMessage("Hello OwO /");

        singletonSE01.printMsg();
        singletonSE02.printMsg();
    }
}
