package enoxs.com.se.java.util;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarSE {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

    @Test
    public void initCalendarTime() {
        Calendar cal = Calendar.getInstance();
        String actual = sdf.format(cal.getTime());
        System.out.println(actual);
    }

    @Test
    public void addDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        String actual = sdf.format(cal.getTime());
        System.out.println(actual);
    }

    @Test
    public void setDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        String actual = sdf.format(cal.getTime());
        System.out.println(actual);
    }

    @Test
    public void setTodayTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 17);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 000);
        String actual = sdf.format(cal.getTime());
        System.out.println(actual);
    }
}
