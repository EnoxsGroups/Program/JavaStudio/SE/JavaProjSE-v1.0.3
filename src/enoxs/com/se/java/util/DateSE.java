package enoxs.com.se.java.util;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.TestCase.assertTrue;

public class DateSE {
    SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void initDateTime() {
        Date date = new Date();
        String actual = sdfTime.format(date);
        System.out.println(actual);
    }

    @Test
    public void hour12Format() throws ParseException {
        Date date = sdfTime.parse("2019-12-01 17:00:00.000");
        String msg = new SimpleDateFormat("hh:mm:ss").format(date);
        System.out.println(msg);
    }

    @Test
    public void hour24Format() throws ParseException {
        Date date = sdfTime.parse("2019-12-01 17:00:00.000");
        String msg = new SimpleDateFormat("HH:mm:ss").format(date);
        System.out.println(msg);
    }

    @Test
    public void before() throws ParseException {
        String first = "2019-12-01";
        String second = "2019-12-15";
        Date dateFirst = sdfDate.parse(first);
        Date dateSecond = sdfDate.parse(second);
        Boolean isTrue = dateFirst.before(dateSecond);
        assertTrue(isTrue);
    }

    @Test
    public void after() throws ParseException {
        String first = "2019-12-01 01:15:01.000";
        String second = "2019-12-01 01:01:01.000";
        Date dateFirst = sdfTime.parse(first);
        Date dateSecond = sdfTime.parse(second);
        Boolean isTrue = dateFirst.after(dateSecond);
        assertTrue(isTrue);
    }

}
