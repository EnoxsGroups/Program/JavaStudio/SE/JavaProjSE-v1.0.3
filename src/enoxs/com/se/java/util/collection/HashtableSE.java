package enoxs.com.se.java.util.collection;

import org.junit.Test;

import java.util.Hashtable;

import static org.junit.Assert.assertEquals;

/**
 * Java的 Hashtable與HashMap的區別如下。
 *
 * Hashtable 不能以null為key；HashMap可以null為key。
 * Hashtable 是執行緒安全的；HashMap是非執行緒安全的。
 * Hashtable 效能比HashMap差。因為Hashtable為執行緒安全，也就是synchronized的，所以在單執行緒時的效能比非執行緒安全的HashMap差。
 */
public class HashtableSE {

    @Test
    public void initHastable(){
        Hashtable hashtable = new Hashtable(3);
        System.out.println(hashtable);
    }

    @Test
    public void hashtableTest(){
        Hashtable hashtable = new Hashtable();
        hashtable.put(1,"1");
        hashtable.put("key","value");

        String actual,expect;

        expect = "{key=value, 1=1}";
        actual = hashtable.toString();
        assertEquals(expect,actual);

        expect = "1";
        actual = hashtable.get(1) + "";
        assertEquals(expect,actual);

        expect = "value";
        actual = hashtable.get("key") + "";
        assertEquals(expect,actual);

        hashtable.clear();

        System.out.println(hashtable);

    }

    @Test
    public void listAllHashTableKey(){
        Hashtable htParam = new Hashtable();
        htParam.put("A","A001");
        htParam.put("B","B001");
        htParam.put("C","C001");
        htParam.put("D","D001");
        htParam.put("E","E001");
        for (Object obj : htParam.keySet()) {
            String key = (String) obj;
            System.out.println(key);
        }

        System.out.println(htParam);




    }

}
