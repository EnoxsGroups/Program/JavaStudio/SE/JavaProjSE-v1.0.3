package enoxs.com.se.java.util.collection;

import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ArraysSE {

    @Test
    public void printArray() {
        String[] strings = {"OwO", "Hello ", "World"};
        String expext = "[OwO, Hello , World]";
        String actual = Arrays.toString(strings);
        assertEquals(expext,actual);
    }

    @Test
    public void sortArray() {
        Integer[] positions = {1, 5, 7, 3, 9};
        Arrays.sort(positions);
        Integer[] expect = {1, 3, 5, 7, 9};
        assertArrayEquals(expect, positions);
    }

    @Test
    public void binarySearch(){
        Integer[] sources = {1, 3, 5, 7, 9};
        Integer target = 5 ;
        int index = Arrays.binarySearch(sources, target);
        int expect = 2;
        assertEquals(expect,index);
    }
    @Test
    public void asList(){
        String [] strings = {"001","002","003"};

        List<String> lstStrings = Arrays.asList(strings);
        List<String> expect = new LinkedList(){{
            add("001");
            add("002");
            add("003");
        }};

        assertEquals(expect,lstStrings);
    }

    @Test
    public void contains(){
        String [] strings = {"001","002","003","004","005"};
        List<String> lstStrings = Arrays.asList(strings);
        boolean isTrue = lstStrings.contains("003");
        assertTrue(isTrue);
    }
}
