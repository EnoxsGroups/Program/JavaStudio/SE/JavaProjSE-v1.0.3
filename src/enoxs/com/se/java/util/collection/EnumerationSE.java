package enoxs.com.se.java.util.collection;

import org.junit.Test;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class EnumerationSE {


    @Test
    public void initEnumerationTest01() {
        Enumeration days;
        Vector dayNames = new Vector();
        dayNames.add("Sunday");
        dayNames.add("Monday");
        dayNames.add("Tuesday");
        dayNames.add("Wednesday");
        dayNames.add("Thursday");
        dayNames.add("Friday");
        dayNames.add("Saturday");
        days = dayNames.elements();
        while (days.hasMoreElements()){
            System.out.println(days.nextElement());
        }
    }
    @Test
    public void initEnumerationTest02(){
        Enumeration param;
        Hashtable htParam = new Hashtable();
        htParam.put("A","A001");
        htParam.put("B","B001");
        htParam.put("C","C001");
        htParam.put("D","D001");
        htParam.put("E","E001");
        Vector lstName = new Vector();
        for (Object obj : htParam.keySet()) {
            String key = (String) obj;
            lstName.add(key);
        }
        param = lstName.elements();
        while (param.hasMoreElements()){
            System.out.println(param.nextElement());
        }
    }

}

