package enoxs.com.se.java.util.collection;

import org.junit.Test;

import java.util.Hashtable;
import java.util.Vector;

import static org.junit.Assert.assertEquals;

public class VectorSE {
    @Test
    public void initVector(){
        Vector vector = new Vector(3);
        System.out.println(vector);
    }

    @Test
    public void vectorTest(){
        Vector vector = new Vector();
        vector.add("1");
        vector.add("2");

        String actual,expect;
        expect = "[1, 2]";
        actual = vector.toString();
        assertEquals(expect,actual);

        expect = "1";
        actual = vector.get(0) + "";
        assertEquals(expect,actual);

        expect = "2";
        actual = vector.get(1) + "";
        assertEquals(expect,actual);

        vector.clear();

        System.out.println(vector);
    }

    @Test
    public void vectorHastTableTest(){
        Vector lstVector = new Vector();
        Hashtable detail01 = new Hashtable();
        detail01.put("A01","AAA");
        detail01.put("A02","AAB");
        detail01.put("A03","ABA");
        detail01.put("A04","BAA");
        detail01.put("A05","ABB");
        lstVector.add(detail01);

        Hashtable detail02 = new Hashtable();
        detail02.put("B01","AAA");
        detail02.put("B02","AAB");
        detail02.put("B03","ABA");
        detail02.put("B04","BAA");
        detail02.put("B05","ABB");
        lstVector.add(detail02);

        System.out.println(lstVector);

        String msg = ((Hashtable)lstVector.get(0)).get("A01")+"";
        System.out.println(msg);
    }
}
