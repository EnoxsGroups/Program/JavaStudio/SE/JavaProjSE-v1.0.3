package enoxs.com.se.java.util.collection;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ListSE {
    @Test
    public void initListObject() {
        //: 無法新增刪除元素
        List<Integer> lstResult = Arrays.asList(1, 2, 3);
        List<Integer> lstExpect = new LinkedList<Integer>(){{
            add(1);add(2);add(3);
        }};
        assertEquals(lstExpect,lstResult);

        //: Java 8
        lstResult = Stream.of(1, 2, 3).collect(Collectors.toList());
        assertEquals(lstExpect,lstResult);
    }

    @Test
    public void mergeTwoList() {
        List<Integer> lstResult = new LinkedList<Integer>() {{
                add(1);
                add(2);
                add(3);
        }};
        List<Integer> lstNumber = new LinkedList<Integer>(){{
            add(4);
            add(5);
            add(6);
        }};
        lstResult.addAll(lstNumber);
        List<Integer> lstExpect = new LinkedList<Integer>(){{
            add(1);
            add(2);
            add(3);
            add(4);
            add(5);
            add(6);
        }} ;
        assertEquals(lstExpect,lstResult);
    }

    @Test
    public void operateListElement(){
        List<Integer> lstResult = new LinkedList<Integer>() {{
            add(1);
            add(2);
            add(3);
        }};
        List<Integer> lstExpect = null;

        lstResult.add(4);
        lstExpect = Arrays.asList(1,2,3,4);
        assertEquals(lstExpect,lstResult);

        lstResult.add(0,0);
        lstExpect = Arrays.asList(0,1,2,3,4);
        assertEquals(lstExpect,lstResult);

        lstResult.remove(0);
        lstExpect = Arrays.asList(1,2,3,4);
        assertEquals(lstExpect,lstResult);

        lstResult.remove(new Integer(4));
        lstExpect = Arrays.asList(1,2,3);
        assertEquals(lstExpect,lstResult);
    }
    @Test
    public void toArray(){
        List<String> lstMsg = new LinkedList<String>(){{
            add("A");
            add("B");
            add("C");
        }};
        Object [] messages = lstMsg.toArray();
        String actual = Arrays.toString(messages);
        String expect = "[A, B, C]";
        assertEquals(expect,actual);
    }

    @Test
    public void queryListMapObject(){
        Map<Integer,String> map1 = new LinkedHashMap<Integer, String>(){{
            put(1,"A1");
            put(2,"A2");
            put(3,"A3");
        }};
        Map<Integer,String> map2 = new LinkedHashMap<Integer, String>(){{
            put(1,"B1");
            put(2,"B2");
        }};
        Map<Integer,String> map3 = new LinkedHashMap<Integer, String>(){{
            put(1,"C1");
        }};
        List<Map<Integer,String>> lstMap = new LinkedList<Map<Integer,String>>(){{
            add(map1);
            add(map2);
            add(map3);
        }};

        List<String> lstResult =
                lstMap.stream()
                .flatMap(each -> each.entrySet().stream())
                .filter(each -> each.getValue().equals("B1"))
//                        .collect(Collectors.toCollection(ArrayList::new));
                .map(each -> each.getValue())
                .collect(Collectors.toList());
        System.out.println(Arrays.toString(lstResult.toArray()));
    }


}
