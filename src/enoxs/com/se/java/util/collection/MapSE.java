package enoxs.com.se.java.util.collection;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MapSE {

    private static Map<Integer, String> map = new HashMap<Integer, String>(){{
        put(1, "JavaProjSE-v1.03");
        put(2, "JUnitSE");
        put(3, "SpringMVC-SE");
    }};

    @Test
    public void initMapObject() {
        Map<Integer, String> map = new HashMap<Integer, String>(){{
            put(1, "JavaProjSE-v1.03");
            put(2, "JUnitSE");
            put(3, "SpringMVC-SE");
        }};
        System.out.println(map);
    }
    @Test
    public void showMapAllKeyAndValue(){
        StringBuffer msgBuf = new StringBuffer(64);
        for (Integer key : map.keySet()) {
            msgBuf.append("key = ");
            msgBuf.append(key);
            msgBuf.append(" , ");
            msgBuf.append("vlaue = ");
            msgBuf.append(map.get(key));
            msgBuf.append("\n");
        }
        System.out.println(msgBuf.toString());
    }
    @Test
    public void putAll(){
        Map<Integer, String> src01 = new HashMap<Integer, String>(){{
            put(1, "JavaProjSE-v1.03");
            put(2, "JUnitSE");
            put(3, "SpringMVC-SE");
        }};
        Map<Integer, String> src02 = new HashMap<Integer, String>(){{
            put(4, "HibernateSE");
            put(5, "ServletJSP-SE");
        }};
        src01.putAll(src02);
        String expect = "{1=JavaProjSE-v1.03, 2=JUnitSE, 3=SpringMVC-SE, 4=HibernateSE, 5=ServletJSP-SE}";
        String actual = src01.toString();
        assertEquals(expect,actual);
    }
}
