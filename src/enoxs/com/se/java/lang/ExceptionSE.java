package enoxs.com.se.java.lang;

import org.junit.Test;

public class ExceptionSE {
    @Test
    public void tryCatchRuntimeExceptionTest(){
        try {
            throw new RuntimeException("Crash !!!");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void tryCatchNumberFormatExceptionTest(){
        try {
            throw new NumberFormatException("Crash !!!");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
