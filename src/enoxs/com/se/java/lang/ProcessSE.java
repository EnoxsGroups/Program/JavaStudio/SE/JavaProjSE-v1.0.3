package enoxs.com.se.java.lang;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProcessSE {
    @Test
    public void testExecCommandLine(){
        String command = "/bin/ls";
        execCommand(command);
    }

    @Test
    public void testExecShellScript(){
        String command = "sh assets/test_exec.sh";
        execCommand(command);
    }

    private void execCommand(Object command){
        try {
            Process process = Runtime.getRuntime().exec((String) command);
            printMsg(process);
            process.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printMsg(Process process){
        try {
            String line = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
