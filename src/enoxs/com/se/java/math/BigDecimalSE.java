package enoxs.com.se.java.math;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BigDecimalSE {
    @Test
    public void initBigDecimal(){
        BigDecimal actual = new BigDecimal("100000");
        BigDecimal expect = new BigDecimal(100000);
        assertEquals(expect,actual);
    }
    @Test
    public void equal(){
        BigDecimal num01 = new BigDecimal(100000);
        BigDecimal num02 = new BigDecimal(100000);
        Boolean isTrue = num01.equals(num02);
        assertTrue(isTrue);
    }

    @Test
    public void compareTo(){
        BigDecimal num01 = new BigDecimal(100000);
        BigDecimal num02 = new BigDecimal(100000);
        int actual = num01.compareTo(num02);
        int expect = 0;
        assertEquals(expect,actual); // 0 : 等於

        num01 = new BigDecimal(90000);
        num02 = new BigDecimal(100000);

        actual = num01.compareTo(num02);
        expect = -1;
        assertEquals(expect,actual); // -1 : 小於

        num01 = new BigDecimal(120000);
        num02 = new BigDecimal(100000);

        actual = num01.compareTo(num02);
        expect = 1;
        assertEquals(expect,actual); // 1 : 大於

    }



}
