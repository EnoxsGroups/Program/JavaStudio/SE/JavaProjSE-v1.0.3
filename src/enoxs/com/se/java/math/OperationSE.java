package enoxs.com.se.java.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OperationSE {
    @Test
    public void calXORTest() {
        Integer source01 = 7;
        Integer source02 = 8;
        Integer actual = source01 ^ source02;
        Integer expect = 15;
        assertEquals(expect, actual);
    }

    @Test
    public void tableANDTest() {
        Integer[] num01 = {0, 0, 1, 1};
        Integer[] num02 = {0, 1, 0, 1};
        Integer result;
        StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < num02.length; i++) {
            result = num01[i] & num02[i];
            sb.append(num01[i]);
            sb.append(" AND ");
            sb.append(num02[i]);
            sb.append(" = ");
            sb.append(result);
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
    @Test
    public void tableORTest() {
        Integer[] num01 = {0, 0, 1, 1};
        Integer[] num02 = {0, 1, 0, 1};
        Integer result;
        StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < num02.length; i++) {
            result = num01[i] | num02[i];
            sb.append(num01[i]);
            sb.append(" OR ");
            sb.append(num02[i]);
            sb.append(" = ");
            sb.append(result);
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
    @Test
    public void tableXORTest() {
        Integer[] num01 = {0, 0, 1, 1};
        Integer[] num02 = {0, 1, 0, 1};
        Integer result;
        StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < num02.length; i++) {
            result = num01[i] ^ num02[i];
            sb.append(num01[i]);
            sb.append(" XOR ");
            sb.append(num02[i]);
            sb.append(" = ");
            sb.append(result);
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

}
