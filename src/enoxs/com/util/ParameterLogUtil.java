package enoxs.com.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.Vector;

public class ParameterLogUtil {
    public static String info(String paramName, Hashtable htResult) {
        StringBuffer sb = new StringBuffer(512);
        sb.append("[" + paramName + "]");
        sb.append(" , ");
        sb.append("(" + htResult.getClass().getName() + ") \n\n");
        for (Object objKey : htResult.keySet()) {
            String key = (String) objKey;
            Object val = htResult.get(objKey);
            String type = htResult.get(objKey).getClass().getName();

            if (htResult.get(objKey) instanceof Vector) {
                sb.append("[");
                sb.append(key);
                sb.append("]");
                sb.append("=");
                sb.append("(" + type + ")");
                sb.append("\n");
                Vector vResult = (Vector) htResult.get(objKey);
                for (Object v : vResult) {
                    sb.append("{");
                    if (v instanceof String) {
                        sb.append(v);
                    }
                    sb.append("}\n");
                }
                sb.append("\n");
            } else {
                sb.append("[");
                sb.append(key);
                sb.append("]");
                sb.append("=");
                sb.append("{");
                sb.append(val);
                sb.append("}");
                sb.append("(" + type + ")");
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public static String info(String paramName, Vector vResult) {
        StringBuffer sb = new StringBuffer(512);
        sb.append("[");
        sb.append(paramName);
        sb.append("]");
        sb.append(" , ");
        sb.append("(" + vResult.getClass().getName() + ")\n\n");

        for (Object res : vResult) {

            if (res instanceof String) {
                sb.append("{");
                sb.append(res);
                sb.append("}");
            }

            if (res instanceof Hashtable) {
                Hashtable htResult = (Hashtable) res;
                for (Object objKey : htResult.keySet()) {
                    String key = (String) objKey;
                    Object val = htResult.get(objKey);
                    String type = htResult.get(objKey).getClass().getName();
                    sb.append("[");
                    sb.append(key);
                    sb.append("]");
                    sb.append("=");
                    sb.append("{");
                    sb.append(val);
                    sb.append("}");
                    sb.append("(" + type + ")");
                    sb.append("");
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }

    public static String info(String paramName, String param) {
        StringBuffer sb = new StringBuffer(512);
        sb.append("[");
        sb.append(paramName);
        sb.append("]");
        sb.append(" , ");
        sb.append("(" + param.getClass().getName() + ")\n");
        sb.append("{");
        sb.append(param);
        sb.append("}\n");
        return sb.toString();
    }

    public static String error(Exception e) {
        StringBuffer sb = new StringBuffer(512);
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        sb.append(e.getMessage());
        sb.append("\n");
        sb.append("exception= \n ");
        sb.append(errors.toString());
        return sb.toString();
    }
}
