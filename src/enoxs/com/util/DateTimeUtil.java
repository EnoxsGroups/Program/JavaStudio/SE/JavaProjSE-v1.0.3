package enoxs.com.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class DateTimeUtil {
    private static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

    private static SimpleDateFormat minSdfDate = new SimpleDateFormat("yyyyMMdd");

    /**
     * 轉換日期格式，由 yyyyMMdd 換成 yyyy-MM-dd
     */
    public static String convert2Date(String strDate){
        String mDate = "";
        try {
            Date mStrDate = minSdfDate.parse(strDate);
            mDate = sdfDate.format(mStrDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDate;
    }

    /**
     * 轉換日期格式，由 yyyy-MM-dd 換成 yyyyMMdd
     */
    public static String convert2MinDate(String strDate) {
        String minDate = "";
        try {
            Date mDate = sdfDate.parse(strDate);
            minDate = minSdfDate.format(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return minDate;
    }

    /**
     * 依據日期排序傳入 Map < Object.key , Object.date>
     * 回傳排序過的 Object Key 陣列
     */
    public static String[] sortSequenceByDate(Map<String, String> map) {
        String[] codes = new String[map.size()];
        Long[] ms = new Long[map.size()];
        try {
            int index = 0;
            String mDate;
            for (String code : map.keySet()) {
                codes[index] = code;
                mDate = map.get(code);
                ms[index] = sdfDate.parse(mDate).getTime();
                index++;
            }

            // bubbleSort;
            int len = ms.length;
            long tmpMs;
            String tmpCode;
            for (int i = 0; i < len; i++) {
                for (int j = 1; j < len; j++) {
                    if (ms[j - 1] > ms[j]) {
                        tmpMs = ms[j - 1];
                        tmpCode = codes[j - 1];

                        ms[j - 1] = ms[j];
                        codes[j - 1] = codes[j];

                        ms[j] = tmpMs;
                        codes[j] = tmpCode;
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return codes;
    }
}
