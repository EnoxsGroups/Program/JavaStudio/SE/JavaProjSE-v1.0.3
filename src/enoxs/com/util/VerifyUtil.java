package enoxs.com.util;

public class VerifyUtil {
    public static boolean isStr(Object obj){
        return obj instanceof String;
    }

    public static boolean isBool(Object obj){
        return obj instanceof Boolean;
    }

    public static boolean isInt(Object obj){
        return obj instanceof Integer;
    }

    public static boolean isVector(Object obj){
        return obj instanceof java.util.Vector;
    }

    public static boolean isHastTable(Object obj){
        return obj instanceof java.util.Hashtable;
    }

}
