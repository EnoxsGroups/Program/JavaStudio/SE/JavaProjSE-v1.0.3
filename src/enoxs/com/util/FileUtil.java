package enoxs.com.util;

import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;
import java.util.List;

/**
 * FileUtil 封裝 util.io.File
 */
public class FileUtil {
    private final static byte[] fileBuffer = new byte[1024 * 5]; //ByteBuffer

    /**
     * 判斷檔案或資料夾是否存在
     */
    public static Boolean isExist(File file) {
        return file.exists();
    }

    /**
     * 檢查表示此抽象路徑名的文件是否是一個檔案
     */
    public static Boolean isFile(File file) {
        return file.isFile();
    }

    /**
     * 檢查表示此抽象路徑名的文件是否是一個目錄
     */
    public static Boolean isDirectory(File file) {
        return file.isDirectory();
    }

    /**
     * 建立資料夾
     */
    public static void makeDirectory(File file) {
        if (!isExist(file)) {
            file.mkdir();
        }
    }

    /**
     * 建立檔案
     */
    public static void makeFile(File file) {
        if (!isExist(file)) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 儲存檔案
     */
    public static void save(File file, byte[] data) {
        makeDirectory(file.getParentFile());
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(data);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 載入檔案
     */
    public static byte[] load(File file) {
        byte[] bytesArray = new byte[(int) file.length()];
        if (isExist(file)) {
            try {
                FileInputStream fis = new FileInputStream(file);
                fis.read(bytesArray); //read file into bytes[]
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("檔案不存在，請檢查");
        }
        return bytesArray;
    }

    /**
     * 更名檔案、更名資料夾
     */
    public static Boolean rename(String name, File source) {
        Boolean isResult = false;
        name = name + "." + FilenameUtils.getExtension(source.getAbsolutePath());
        File fileResult = new File(source.getParentFile(), name);
        if (isExist(source)) {
            isResult = source.renameTo(fileResult);
            if (!isResult) {
                System.out.println("重新命名失敗");
            }
        } else {
            System.out.println("檔案不存在,重新命名失敗");
        }
        return isResult;
    }

    /**
     * 複製檔案
     */
    public static Boolean copy(File target, File source) {
        makeDirectory(new File(target.getParent()));
        try {
            if (isExist(source)) {
                int length;
                InputStream input = new FileInputStream(source.getAbsolutePath()); // 讀入原檔
                FileOutputStream output = new FileOutputStream(target.getAbsolutePath());// 目標路徑

                while ((length = input.read(fileBuffer)) != -1) {
                    output.write(fileBuffer, 0, length);
                }
                output.flush();
                output.close();
                input.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 複製文件夾
     */
    public static void copy(String targetPath, String sourcePath) {

    }

    public static void delete(File file) {
        try {
            if (isExist(file)) {
                if (isFile(file)) {
                    file.delete();
                }
                if (isDirectory(file)) {
                    file.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<File> getPathDirectory(String path) {
        List<File> lstFile = new LinkedList<>();
        File file = new File(path);
        String[] lstName = file.list();
        for (String name : lstName) {
            lstFile.add(new File(path + File.separator + name));
        }
        return lstFile;
    }

    public static Long createTime(File file) {
        Path mPath = Paths.get(file.getAbsolutePath());
        BasicFileAttributes attrs;
        Long time = Long.valueOf(-1);
        try {
            attrs = Files.readAttributes(mPath,BasicFileAttributes.class);
            time = attrs.creationTime().toMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return time;
    }

    public static Long modifyTime(File file) {
        Path mPath = Paths.get(file.getAbsolutePath());
        BasicFileAttributes attrs;
        Long time = Long.valueOf(-1);
        try {
            attrs = Files.readAttributes(mPath,BasicFileAttributes.class);
            time = attrs.lastModifiedTime().toMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return time;
    }

    public static Long accessTime(File file) {
        Path mPath = Paths.get(file.getAbsolutePath());
        BasicFileAttributes attrs;
        Long time = Long.valueOf(-1);
        try {
            attrs = Files.readAttributes(mPath,BasicFileAttributes.class);
            time = attrs.lastAccessTime().toMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return time;
    }

    //Enoxs To-Do : 複製文件夾
    //Enoxs To-Do : 刪除檔案
    //Enoxs To-Do : 刪除文件夾
    //Enoxs To-Do : 移動檔案
}
