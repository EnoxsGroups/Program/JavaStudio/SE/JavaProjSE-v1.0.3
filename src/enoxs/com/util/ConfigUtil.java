package enoxs.com.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class ConfigUtil {
    public static Properties loadConfigProps(String configPath) {
        Properties props = new Properties();
        try {
            FileInputStream fis = new FileInputStream(configPath);

            BufferedInputStream bis = new BufferedInputStream(fis);
            InputStreamReader in = new InputStreamReader(bis, "utf-8");

            props.load(in);

            fis.close();
            bis.close();
            in.close();
            fis = null;
            bis = null;
            in = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }
}
