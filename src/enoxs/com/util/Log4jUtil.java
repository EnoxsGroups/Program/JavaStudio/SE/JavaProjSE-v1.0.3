package enoxs.com.util;

import org.apache.log4j.PropertyConfigurator;

import java.util.Properties;

/**
 * Msg.log
 *
 * Add:
 * private static Logger log = Logger.getLogger(ClassName.class);
 *
 * Depend ConfigUtil
 */
public class Log4jUtil {
    public static void loadLog4jProps(String configPath) {
        Properties prob = ConfigUtil.loadConfigProps(configPath);
        PropertyConfigurator.configure(prob);
    }
}
