package enoxs.com.util;

import org.junit.Test;

import java.util.List;
import java.util.Map;

public class XmlFormatUtil {
    /**
     * 組成 HttpServletResponse 回應所需 XML 格式化
     */
    public static String formatXML(Map<String, Object> responseParameter) {
        StringBuffer sbXMLContent = new StringBuffer(256);
        try {
            sbXMLContent.append("<row>");
            if (responseParameter != null) {
                for (String name : responseParameter.keySet()) {
                    if (!"t".equals(name)) {
                        if (responseParameter.get(name) instanceof java.util.List) {
                            combinListObjectXML(sbXMLContent, name, responseParameter.get(name));
                        } else {
                            combinCustomObjectXML(sbXMLContent, name, responseParameter.get(name) + "");
                        }
                    }
                }
            }
            sbXMLContent.append("</row>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sbXMLContent.toString();
    }

    private static StringBuffer combinListObjectXML(StringBuffer sbMsg, String name, Object value) {
        List<Object> lstObject = (List<Object>) value;

        appendXMLHeader(sbMsg, name);
        for (Object obj : lstObject) {
            sbMsg.append("<list name='list'>");
            if (obj.getClass().getName().equals("java.lang.String")) {
                sbMsg.append((String) obj);
            } else {
                Map<String, Object> map = DataTypeUtil.beanToMap(obj);
                for (String mName : map.keySet()) {
                    if (map.get(mName) instanceof java.util.List) {
                        combinListObjectXML(sbMsg, mName, map.get(mName));
                    } else {
                        combinCustomObjectXML(sbMsg, mName, map.get(mName) + "");
                    }
                }
            }
            sbMsg.append("</list>");
        }
        appendXMLFooter(sbMsg, name);

        return sbMsg;
    }

    private static StringBuffer combinCustomObjectXML(StringBuffer sbMsg, String name, String value) {
        appendXMLHeader(sbMsg, name);
        sbMsg.append(value);
        appendXMLFooter(sbMsg, name);
        return sbMsg;
    }

    /**
     * Call By Reference
     */
    private static void appendXMLHeader(StringBuffer sb, String header) {
        sb.append("<");
        sb.append(header);
        sb.append(" name=");
        sb.append("\'");
        sb.append(header);
        sb.append("\'");
        sb.append(">");
    }

    private static void appendXMLFooter(StringBuffer sb, String footer) {
        sb.append("</");
        sb.append(footer);
        sb.append(">");
    }
}
