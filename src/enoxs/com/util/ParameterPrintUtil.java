package enoxs.com.util;

import java.io.File;
import java.util.Hashtable;
import java.util.Vector;

public class ParameterPrintUtil {
    private static String filePath;

    public static void print(String title , Hashtable htResult){
        StringBuffer sb = new StringBuffer(512);
        sb.append("["+ title + "] \n");

        for (Object objKey : htResult.keySet()) {
            String key = (String) objKey;
            Object val = htResult.get(objKey);
            String type = htResult.get(objKey).getClass().getName();

            if(htResult.get(objKey) instanceof Vector){
                sb.append("[");
                sb.append(key);
                sb.append("]");
                sb.append("=");
                sb.append("(" + type + ")");
                sb.append("\n");
                Vector vResult = (Vector) htResult.get(objKey) ;
                for (Object v : vResult) {
                    sb.append("{");
                    if(v instanceof java.lang.String){
                        sb.append(v);
                    }
                    sb.append("}\n");
                }
            }else{
                sb.append("[");
                sb.append(key);
                sb.append("]");
                sb.append("=");
                sb.append("{");
                sb.append(val);
                sb.append("}");
                sb.append("(" + type + ")");
                sb.append("\n");
            }
        }
        sb.append(" ====== ====== ====== \n\n\n");
        System.out.println(sb.toString());
        report(sb.toString());
    }

    public static void print(String title, Vector vResult){
        StringBuffer sb = new StringBuffer(512);
        sb.append("[");
        sb.append(title);
        sb.append("]");
        sb.append("=");
        sb.append("(" + vResult.getClass().getTypeName() + ")");
        sb.append("\n");
        for (Object v : vResult) {
            if(v instanceof java.lang.String){
                sb.append("{");
                sb.append(v);
                sb.append("}\n");
            }
            if(v instanceof java.util.Hashtable){
                Hashtable htResult = (Hashtable) v;
                for (Object objKey : htResult.keySet()) {
                    String key = (String) objKey;
                    Object val = htResult.get(objKey);
                    String type = htResult.get(objKey).getClass().getName();
                    sb.append("[");
                    sb.append(key);
                    sb.append("]");
                    sb.append("=");
                    sb.append("{");
                    sb.append(val);
                    sb.append("}");
                    sb.append("(" + type + ")");
                    sb.append("\n");
                }
            }
        }
        sb.append(" ====== ====== ====== \n\n\n");
        System.out.println(sb.toString());
        report(sb.toString());
    }

    public static void setPath(String path){
        filePath = path;
    }

    private static void report(String msg){
        File file = new File(filePath);
        FileUtil.save(file,msg.getBytes());
    }
}
