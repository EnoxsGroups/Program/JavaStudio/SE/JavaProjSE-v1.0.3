package enoxs.com.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * 資料型態轉換
 */
public class DataTypeUtil {
    /**
     * Object 轉換為 Map 型態
     */
    public static Map<String, Object> convertObject2Map(Object object) {
        Map<String, Object> map = new HashMap<>();

        Class<?> clazz = object.getClass();

        Field[] fields = clazz.getDeclaredFields();
        String key;
        Object value;

        for (Field field : fields) {
            key = field.getName();
            try {
                Field var = clazz.getDeclaredField(key);
                var.setAccessible(true);
                value = var.get(object);
                map.put(key, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println(map);
        return map;
    }

    /**
     * 安全檢查不通過，改用 Bean 轉 Map
     * field.setAccessible(true);
     * Reference:
     * https://blog.csdn.net/sunlihuo/article/details/83687319
     */

    public static Map<String, Object> beanToMap(Object obj) {
        if (obj == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                if (!key.equals("class")) {
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(obj);
                    if(value == null){
                        value = "";
                    }
                    map.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }


    public static Hashtable beanToHashtable(Object obj) {
        if (obj == null) {
            return null;
        }
        Hashtable hashtable = new Hashtable();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                if (!key.equals("class")) {
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(obj);
                    hashtable.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashtable;
    }

    /**
     * Map 轉換為 Object 型態
     */
    public static Object convertMap2Object(Map<String, Object> map , Class<?> beanClass) throws Exception {
        if (map == null)
            return null;

        Object obj = beanClass.newInstance();

        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            int mod = field.getModifiers();
            if(Modifier.isStatic(mod) || Modifier.isFinal(mod)){
                continue;
            }

            field.setAccessible(true);
            field.set(obj, map.get(field.getName()));
        }

        return obj;
    }

    public static Object mapToBean(Map<String, Object> map, Class<?> beanClass) throws Exception {
        if (map == null)
            return null;

        Object obj = beanClass.newInstance();

        BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor property : propertyDescriptors) {
            Method setter = property.getWriteMethod();
            if (setter != null) {
                setter.invoke(obj, map.get(property.getName()));
            }
        }

        return obj;
    }

    public static Map<String,Object> filterMapNullValue(Map<String,Object> map){
        for (String code : map.keySet()) {
            if(map.get(code) == null || "null".equals(map.get(code))){
                map.put(code,"");
            }
        }
        return map;
    }
}
