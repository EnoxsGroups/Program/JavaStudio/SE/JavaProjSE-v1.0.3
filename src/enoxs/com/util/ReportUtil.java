package enoxs.com.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ReportUtil {
    private StringBuffer sbMsg = new StringBuffer(512);
    private String reportName = "";
    private String exportPath = "";

    private String lv01TitleLine = "\n======\n"; // #
    private String lv02TitleLine = "\n------\n"; // ##
    private String lv03TitleMark = "\n### "; // ###
    private String line = "\n---\n"; // ---
    private String logTag =  "\n```\n" ; //```
    private String chkY =  "+ [X] " ; // + [X] item
    private String chkN =  "+ [ ] " ; // + [ ] item
    private String mdItem = "+ ";
    private Integer registerIndex = 0;
    private Map<Integer,String> lstRegister = new HashMap<>();

    public ReportUtil() {
    }

    public ReportUtil(String reportName) {
        this.reportName = reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setExportPath(String exportPath) {
        this.exportPath = exportPath;
    }

    public void addLv01Title(String title){
        sbMsg.append(title);
        sbMsg.append(lv01TitleLine);
    }

    public void addLv02Title(String title){
        sbMsg.append(title);
        sbMsg.append(lv02TitleLine);
    }
    public void addLv03Title(String title){
        sbMsg.append(lv03TitleMark);
        sbMsg.append(title);
        sbMsg.append("\n");
    }

    public void addContent(String content){
        sbMsg.append(content);
        sbMsg.append("\n");
    }

    public void addLog(String log){
        sbMsg.append(logTag);
        sbMsg.append(log);
        sbMsg.append(logTag);
    }

    public void addCheck(String item,Boolean isOk){
        if(isOk){
            sbMsg.append(chkY);
        }else{
            sbMsg.append(chkN);
        }
        sbMsg.append(item);
        sbMsg.append("\n");
    }

    public void addLine(){
        sbMsg.append(line);
    }

    public void addItem(String item){
        sbMsg.append(mdItem);
        sbMsg.append(item);
        sbMsg.append("\n");
    }

    public void addCR(){
        sbMsg.append("\n");
    }


    public void pushRegister(Integer index){ // 0 1 2 3 4 5  ...
        lstRegister.put(index, new String(sbMsg.toString()));
        sbMsg.setLength(0);
        registerIndex = index;
    }

    public void popRegister(){
        sbMsg.setLength(0);
        int len = lstRegister.size();
        for (int i = 0; i < len; i++) {
            sbMsg.append(lstRegister.get(i));
            sbMsg.append("\n\n\n");
        }
        lstRegister.clear();
        registerIndex = 0;
    }

    public void export(){
        String path = exportPath + File.separator +  reportName + ".md";
        File file = new File(path);
        FileUtil.save(file,sbMsg.toString().getBytes());

        sbMsg.setLength(0);
        lstRegister.clear();
    }
}
