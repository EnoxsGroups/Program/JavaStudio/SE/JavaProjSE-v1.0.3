Java Project Simple Example - Version 1.0.3
======
![cover](assets/readme/cover.png)

專案說明
------
Java 程式語言，簡易參考範例。

1. 測試 java language level 8 (含)以下， sdk 中類別「特性」與「使用方法」
2. 封裝常用類別，建置公用性工具。
3. 封裝第三方元件（.jar），建置公用性工具。同時包含該工具之單元測試與使用手冊。

整併過去整理的 Java 工具類別與套件庫範例程式：
+ <https://github.com/RHZEnoxs/JavaProjSE-v1.0.2>

模組說明
------

### Java 主程式
dir: src/enoxs/com/
+ se/
    
        測試 java sdk 中類別「特性」與「使用方法」，使用單元測試工具(junit 4)
+ util/

        通用性工具類別，封裝常用類別或第三方元件

### 單元測試 (test )    
dir: test/enoxs/com/
+ util/
            
        相同目錄結構，單元測試封裝後的 Util 類別。
        
### 使用手冊 (doc)    
dir: doc/enoxs/com/
+ util/
        
        相同目錄結構，說明描述封裝後的 Util 類別。
        使用方法、安裝配置、注意事項...等。
        
    + [ConfigUtil](doc/enoxs/com/util/ConfigUtil.md)
    + [Log4jUtil](doc/enoxs/com/util/Log4jUtil.md)

操作環境
------
+ IDE : IntelliJ
+ SDK : OpenJDK8U